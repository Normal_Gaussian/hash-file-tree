import { hashFileTree, exampleDir } from "./";

it("should hash files with filters", async () => {
  expect.assertions(2);

  const hashes = await hashFileTree(exampleDir, {
    include_dir: "(1|2|3)$",
    exclude_dir: "3$",
    include_file: "^(e|f)",
    exclude_file: "D$"
  });

  expect(Object.keys(hashes).length).toBe(3);

  expect(hashes).toMatchInlineSnapshot(`
    Object {
      "fileA": "d31fa862f513b9868c8e44dbac38330226d253f967b4ef57203343930f80bca8ee4510bd4681e7f812686fbf2b0a74e64116bbfda7f990693d396d52d1a8dd4f",
      "subdirectory1/exampleFileB": "63023ce77871d4e209da51e091aeea564173396a136fe79263b744c3e91c62b6cf6d6e81c9f3ec7f7e575433c4076dc292b14ad9cccc7b9fa7deb7e239218853",
      "subdirectory1/subdirectory2/exampleFileC": "ff9e5ee9a317055569aa56da340d9fd6a938200f61ba5786eac697ef5cd053ad4d9e8cbcc8687a671854e0e716f9b757a5187c246c49d7fde90ae2aa7a07f773",
    }
  `);
});

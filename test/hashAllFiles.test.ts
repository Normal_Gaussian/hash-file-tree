import { hashFileTree, exampleDir } from "./";

it("should hash all files", async () => {
  expect.assertions(2);

  const hashes = await hashFileTree(exampleDir);

  expect(Object.keys(hashes).length).toBe(6);

  expect(hashes).toMatchInlineSnapshot(`
    Object {
      "fileA": "d31fa862f513b9868c8e44dbac38330226d253f967b4ef57203343930f80bca8ee4510bd4681e7f812686fbf2b0a74e64116bbfda7f990693d396d52d1a8dd4f",
      "subdirectory1/exampleFileB": "63023ce77871d4e209da51e091aeea564173396a136fe79263b744c3e91c62b6cf6d6e81c9f3ec7f7e575433c4076dc292b14ad9cccc7b9fa7deb7e239218853",
      "subdirectory1/subdirectory2/exampleFileC": "ff9e5ee9a317055569aa56da340d9fd6a938200f61ba5786eac697ef5cd053ad4d9e8cbcc8687a671854e0e716f9b757a5187c246c49d7fde90ae2aa7a07f773",
      "subdirectory1/subdirectory2/exampleFileD": "18e6d65d31e2a2c9c5436085a9679bc84d25bd01fb69f2143cac856c4bd4eeb31fba0031afdd42bd12c5d4d3de3bf108505932280b694acef348d74ce5af0fea",
      "subdirectory1/subdirectory2/notAnotherFileE": "92ccaffd5a00dab2499ab5b97cbb3b6f81f0f197e66d477e4ff40bed7e6848f4e2630ec18f0b25c0c4392a21f608f2df90efbc94f111b3b3fc90a2b85bb5c5e3",
      "subdirectory1/subdirectory3/hey": "a0bcb70b771de1f614c724a86169288ee9dc749a6c0bbb9dd0f863c2b66531d21b65b81bd3d3ec4e345c2fea59032a1b4f3fe52317da3bf075374f7b699b10aa",
    }
  `);
});

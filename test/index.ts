// A "production" test uses the built assets
// A non production test uses the source files

const modulePath = process.env.NODE_ENV === "production" ? '../dist/index.js' : '../src/index.static.ts';
export const hashFileTree = require(modulePath).default;
export const exampleDir = __dirname + '/exampleDir';

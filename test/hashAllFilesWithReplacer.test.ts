import { hashFileTree, exampleDir } from "./";
import replaceStream from "replacestream";

it("should hash all files with replacer", async () => {
  expect.assertions(2);

  const hashes = await hashFileTree(exampleDir, {
    transformStreamFactory: () => replaceStream("file", "cat")
  });

  expect(Object.keys(hashes).length).toBe(6);

  expect(hashes).toMatchInlineSnapshot(`
    Object {
      "fileA": "e97404091ae6b19f8ad4fb4915005c0cebd4b3bd4d8272f94d5d52206d90fff7c6b38a91bf9193070afef2701b9dbf168bffb6fe823b7c39482e90c19290fb8e",
      "subdirectory1/exampleFileB": "a4d1637b0ab3d7a05eded0bcbba3daed3796eadeac26329377d2062f5e28d289f30b9c3bea335a8b49a58a826114c51e286e4b7d7e2ef8b6139eb1109c3887f9",
      "subdirectory1/subdirectory2/exampleFileC": "a369542d34049f14357bbe074015848ff41c80803325ad0483bdcc65ac18940bc19409fe3d0289c4f602c894e8da3689910e3cad7cce619182dbd6316710f26d",
      "subdirectory1/subdirectory2/exampleFileD": "1ca2ab02eccb133788386a2bcfa0dee8e16b5995f30130b7957785a59b20fbc647f8aa1c14c6c2283b9205dc413ffd07aaf5dd7820bc9498cb46a4e8b86fecbf",
      "subdirectory1/subdirectory2/notAnotherFileE": "590de3aa78fd946b840d22b2fb38d4181ee3597a13258ee7bf7cd1e2a71f3a46b7167eeeabe57a5d7a4fe7979d9ffd8cefd1661323f6e13ae6eec62a651a06a1",
      "subdirectory1/subdirectory3/hey": "a0bcb70b771de1f614c724a86169288ee9dc749a6c0bbb9dd0f863c2b66531d21b65b81bd3d3ec4e345c2fea59032a1b4f3fe52317da3bf075374f7b699b10aa",
    }
  `);
});

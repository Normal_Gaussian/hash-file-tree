import traverse from 'filesystem-traverse';
import hasha from 'hasha';
import path from 'path';
import fs from 'fs';

type Options = {
  include_file?: string | RegExp,
  exclude_file?: string | RegExp,
  include_dir?: string | RegExp,
  exclude_dir?: string | RegExp,
  osPath?: boolean,
  transformStreamFactory?: (filepath: string) => NodeJS.ReadWriteStream
}

export default async function hashFileTree(directory: string, options: Options = {}): Promise<{[path:string]: string}> {
  const promises: Array<Promise<[string, string]>> = await traverse({
    ...options,
    directory,
    process_file: function(filepath: string): Promise<[string, string]> {
      const fullPath = path.join(directory, filepath);
      const displayPath = options?.osPath ? filepath : filepath.split(path.sep).join('/')

      if(options.transformStreamFactory) {
        const transformStream = options.transformStreamFactory(filepath);
        const stream = fs.createReadStream(fullPath).pipe(transformStream);
        return hasha.fromStream(stream)
          .then(hash => [displayPath, hash])
      } else {
        return hasha.fromFile(fullPath)
          .then(hash => [displayPath, hash])
      }
    }
  });

  const traversedFiles = await Promise.all(promises);

  return traversedFiles.reduce((acc, [filepath, hash]) => {
    acc[filepath] = hash;
    return acc;
  }, {});
}